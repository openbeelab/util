util = require 'util'
module.exports =

    (sensor)->

        measure =
            type : 'measure'
            measureOrigin : "automatic"
            name : sensor?.process
            raw_value : null
            value : null
            unit : sensor?.unit
            timestamp : new Date()
            stand :
                _id : sensor?.device?.stand?._id
                name : sensor?.device?.stand?.name
            device : sensor?.device?.name
            sensor : sensor?.name
            isTest : sensor?.isTest
            bias : sensor?.bias or 0
            gain : sensor?.gain or 1

        return measure
