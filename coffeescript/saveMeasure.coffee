module.exports = (measure,dataDb,mustAddServerTimestamp=true)->

    dataDb.save(measure)
    .then (result)->
        
        if mustAddServerTimestamp
            return dataDb.save({ _id : "_design/updates/_update/time/" + result._id })
        else
            return result
