console.log "require cradle"
cradle = require 'cradle'
console.log "require promisify"
promisify_db = require './promisify_dbDriver'
console.log "require promise"
Promise = require 'promise'

exports.connectToServer = (config,options) ->

    if options?.debug
        console.log "db host:" + config.host
        console.log "db name:" + config.name
        console.log "stand id:" + config.stand_id
    
    server = new (cradle.Connection)(config.protocol + '://'+ config.host, config.port,config)
    server.useDb = (name)->
        
        db = promisify_db(@database(name))
        db.name = name
        return db

    server.databases = Promise.denodeify server.databases.bind(server)
    server.config = Promise.denodeify server.config.bind(server)
    server.info = Promise.denodeify server.info.bind(server)
    server.stats = Promise.denodeify server.stats.bind(server)
    server.activeTasks = Promise.denodeify server.activeTasks.bind(server)
    server.uuids = Promise.denodeify server.uuids.bind(server)
    
    return server
