
utils =

    isIn : (collection) ->

        #if @ instanceof Number or @ instanceof String or @ instanceof Boolean

        #return collection.contains(@.valueOf()) or collection.contains(@)
        return collection.contains?(@) or collection.contains?(@.valueOf?()) or false

        #return collection.contains @

utils.install = ->

    Object.afterDelay = (delay,callback)->

        setTimeout(callback,delay)

    for own methodName,method of utils

        if not Object::[methodName]? and methodName isnt "install"

            Object::[methodName] = method

    return
module.exports = utils
