require('../../util/javascript/numberUtils').install()
require('../../util/javascript/stringUtils').install()
cradle = require 'cradle'
promisify_db = require './promisify_dbDriver'
Promise = require 'promise'

currentView = null
currentDoc = null
emit = (key,value)->
    currentView.cache.push {id:currentDoc._id,key:key,value:value}

exports.connectToServer = (config,options) ->

    if options?.debug
        console.log "db host:" + config.host
        console.log "db name:" + config.name
        console.log "stand id:" + config.stand_id
    
    server = {}
    server.dbs = {}
    server.database = server.useDb = server.create
    server.useDb = (name)->

        if not server.dbs[name]
            db = {}
            db.exists = -> return Promise.resolve(server.dbs[name]?)
            db.create = ->
                server.dbs[name] = {docs:{},views:{}}
                Promise.resolve({ok:true})
            return db
        else
            db = server.dbs[name]
            db.exists = -> return Promise.resolve(true)
            db.get = (id) ->
                if id.startsWith "_design"
                    if db.views[id]
                        cache = db.views[id].cache
                        ret = {"total_rows":cache.length,"offset":0,"rows":cache}
                        return Promise.resolve ret
                    else
                        return Promise.reject({ok:false,viewMissing:id})
                if db.docs[id]?
                    _doc = db.docs[id]
                    if _doc.timestamp?
                        _doc.timestamp = new Date(_doc.timestamp)
                    return Promise.resolve(_doc)
                else
                    return Promise.reject({ok:false,docMissing:id})
            db.save = (doc) ->

                if doc.timestamp?
                    doc.timestamp = doc.timestamp.toISOString()
                if not doc._id
                    doc._id = String.generateToken(6)
                if not doc._rev
                    doc._rev = "0-0"
                revNb = doc._rev.split("-")[0].toInt() + 1
                doc._rev = revNb + "-" + String.generateToken(6)
                db.docs[doc._id] = doc

                if not doc._id.startsWith("_design")

                    for own viewName,view of db.views

                        view.cache.clear()
                        currentView = view
                        for own _,dok of db.docs
                            currentDoc = dok
                            view._map(dok)
                else
                    for own viewPartialName,mapReduce of doc.views when doc.views

                        view = {cache: [],id:String.generateToken(3)}
                        viewName = doc._id + "/_view/" + viewPartialName

                        eval("view._map = " + mapReduce.map)
                        if mapReduce.reduce
                            eval("view._reduce = " + mapReduce.reduce)
                        db.views[viewName] = view

                        currentView = view
                        for own _,dok of db.docs
                            currentDoc = dok
                            view._map(dok)
                return Promise.resolve({ok:true,id:doc._id,rev:doc._rev})

        return db

    server.databases = (key for key of server.dbs)
    server.config = -> "server config..."
    server.info = -> "server info..."
    server.stats = -> "server stats..."
    server.activeTasks = -> "server tasks..."
    server.uuids = -> "server uuids..."

    return server
