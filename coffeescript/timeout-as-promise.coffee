'use strict'
Promise = require 'promise'

module.exports = ->
    args = Array.prototype.slice.call(arguments)
    return new Promise (resolve) ->
        setTimeout.apply(null, [resolve].concat(args))
