module.exports = ->
    return {
        location :
            _id : "location:le_bel_ordinaire"
            type : "location"
            name : "le_bel_ordinaire"
            locationType : "GPS"
            latitude : 43.301841
            longitude : -0.399959
            #latitude : 43.379061
            #longitude : -1.751513
            create_noised_area : true
            noise : 0.0166666667 #degree = 1 minute

        beehousemodel :
            _id : "beehousemodel:dadant"
            name : "dadant"
            type : "beehousemodel"
            weight :
                value : 37
                unit : "Kg"
            extra_box_weight :
                value : 5
                unit : "Kg"

        beehouse :
            _id : 'beehouse:ruche_001'
            type : "beehouse"
            name : 'ruche 001'
            model_id : "beehousemodel:dadant"
            location_id : "location:nekatoenea"
            number_of_extra_boxes : 0
            has_roof : true

        stand :

            _id : "stand:socle_001"
            name : ":socle 001"
            type : "stand"
            device : "arietta_g25"
            sensors :
                globalWeightManual :
                    active : false
                    process : "global-weight"
                    bias : 0
                    gain : -1.9
                    unit : 'Kg'
                romanScale :
                    active : true
                    process : "global-weight"
                    motor :
                        power : 'J4.21'
                        disable : 'J4.23'
                        ms1 : 'J4.25'
                        ms2 : 'J4.27'
                        ms3 : 'J4.29'
                        pulse : 'J4.28'
                        direction : 'J4.30'
                        sleep : 'J4.26'
                        reset : 'J4.24'
                        stepDelay : 0.01
                    photoDiode : 'in_voltage0_raw'
                    irDiode : 'J4.31'
                    refValue : 512
                    bias : 0
                    gain : -1.9
                    unit : 'Kg'
                    isTest : false
                rtcTemperature :
                    active : true
                    process : "temperature"
                    bias : 0
                    gain : 1
                    unit : '°C'
                    isTest : false
            sleepMode : true
            sleepDuration : 3300
            beehouse_id : "beehouse:ruche_001"
            #location_id : "location:jbc"
            location_id : "location:nekatoenea"
            weight0_pin : "J4.33"
    }
#       location :
#            _id : "location:jbc"
#            type : "location"
#            name : "jbc"
#            locationType : "GPS"
#            latitude : 43.301854
#            longitude : -0.399957
#            create_noised_area : true
#            noise : 0.0166666667 #degree = 1 minute
