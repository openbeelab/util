utils =

    times : (fn) ->

        for i in [0...@valueOf()]
            fn(i)

    floor : ->

        return Math.floor(@)

    ceil : ->

        return Math.ceil(@)
    
    abs : ->

        return Math.abs(@)

    sign : ->

        return @ && @ / Math.abs(@)

    compareTo : (other) -> return @ - other

    getRandomBetween : (min = 0.0, max = 1.0)->

        return Math.random() * (max - min) + min

    #deprecated, use getRandomBetween
    getRandomArbitrary : (min, max)->

        return @getRandomBetween(min,max)

for own methodName,method of require './comparable'

    utils[methodName] = method

utils.install = ->

    for own methodName,method of utils

        if not Number::[methodName]? and (methodName isnt "install" or methodName isnt "getRandomArbitrary")

            Number::[methodName] = method

    if not Number.getRandomArbitrary?

        Number.getRandomArbitrary = utils.getRandomArbitrary

    if not Number.getRandomBetween?

        Number.getRandomBetween = utils.getRandomBetween
module.exports = utils
