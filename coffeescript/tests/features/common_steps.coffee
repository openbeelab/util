#require('../../objectUtils').install()
world = require './support/world'
world.fixtures = require('../../fixtures')()
expect = require 'must'
#yaml_load = require "../../yaml"

module.exports = () ->

    @Given "an instance of couchdb",  () ->

        dbDriver = require '../../mockDriver'
        world.dbServer = dbServer = dbDriver.connectToServer("mock config placeholder",world.options)
        expect(world.dbServer).to.exist()
        dbServer.useDb("_users").create()
        world.usersDb = dbServer.useDb("_users")
        expect(world.usersDb).to.exist()
        world.usersDb.save {_id:'org.couchdb.user:admin',roles:['admin'],type:'user',name:'admin'}
        .then ->
            world.usersDb.save {_id:'org.couchdb.user:remy',roles:['admin'],type:'user',name:'remy'}

    @Given "an openbeelab system",  () ->

        createBundle = require '../../../../db-admin/javascript/create_bundle'
        config = require '../../../test-config'
        createBundle(config,world.dbServer,world.fixtures)
        .then (logs)=>

            world.configDb = world.dbServer.useDb(config.database.name + "_config")
            world.dataDb = world.dbServer.useDb(config.database.name + "_data")
            world.usersDb = world.dbServer.useDb("_users")


    @Given "the system has a location",  () ->

        world.configDb.get world.fixtures.location._id
        .then (location)->
            world.location = location

    @Given "i measure time from now",  () ->

        world.startTime = new Date()
