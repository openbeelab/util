This repository contains mandatory utilities snippets used by other openbeelab repositories.

For more infos/docs, see [the wiki](https://gitlab.com/openbeelab/docs/wikis/home)